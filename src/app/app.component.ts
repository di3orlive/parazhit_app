import {AfterViewInit, Component, ViewChild, NgZone} from "@angular/core";
import {Platform, ModalController, ToastController, NavController} from "ionic-angular";
import {StatusBar} from "@ionic-native/status-bar";
import {SplashScreen} from "@ionic-native/splash-screen";
import { MusicControls } from '@ionic-native/music-controls';
import { LocalNotifications } from '@ionic-native/local-notifications';
import {HomePage} from '../pages/home/home';
import {ContactPage} from "../pages/contact/contact";
import {EventsPage} from "../pages/events/events";
import {JobsPage} from "../pages/jobs/jobs";
import {NewsPage} from "../pages/news/news";
import {PrivacyPage} from "../pages/policy/policy";
import {PlayModal} from "../modals/play/play";
import {ArtistsPage} from "../pages/artists/artists";
import {ShowsPage} from "../pages/shows/shows";
import {PlayerService} from "../services/player.services";
import {SafeSubscribe} from "../helpers/safe-subscripe/safe-subscripe";

@Component({
  templateUrl: 'app.html'
})
export class MyApp extends SafeSubscribe implements AfterViewInit{
  @ViewChild('nav') nav: NavController;
  @ViewChild('player') player;
  rootPage = HomePage;
  homePage = HomePage;
  jobsPage = JobsPage;
  newsPage = NewsPage;
  eventsPage = EventsPage;
  privacyPage = PrivacyPage;
  contactPage = ContactPage;
  residentsPage = ArtistsPage;
  showsPage = ShowsPage;
  playing = false;
  menuBtn = false;



  constructor(
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    private musicControls: MusicControls,
    private playerService: PlayerService,
    private _ngZone: NgZone,
    private localNotifications: LocalNotifications,
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen
  ) {
    super();
    
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();


      if(!navigator.onLine){
        this.checkInetConnection();
      }
  
  
      this.playerService.playingAsync.safeSubscribe(this, (value) => {
        this.playing = value;
      });
    });
  }
  
  
  
  ngAfterViewInit(){
    this.playerService.setPlayer(this.player);
    this.playerService.setPlaying(this.playing);
  }

  
  setPage(page:any):void{
    this.showModal(false);
    this.nav.push(page);
  }

  setRootPage(page:any):void{
    this.showModal(false);
    this.nav.setRoot(page);
    this.rootPage = page;
  }

  isActivePage(page:any):boolean{
    return this.rootPage == page;
  }

  showModal(val:boolean) {
    this.menuBtn = val;
  }

  playModal(){
    this.playerService.setPlaying(true);
  
    
    
    // this.localNotifications.schedule({
    //   id: 1,
    //   title: 'Local ILocalNotification Example',
    //   text: 'Single ILocalNotification',
    //   sound: 'assets/wilhelm.mp3',
    //   icon: 'assets/logo-c.png',
    //   led: 'ff0000'
    // });
  
    
    
    // for native ONLY
    // this.musicControls.create({
    //   track       : 'test track name',
    //   artist      : 'test artist name',
    //   cover       : 'assets/logo-c.png',
    //   isPlaying   : true,
    //   dismissable : false,
    //   hasPrev   : false,
    //   hasNext   : false,
    //   hasClose  : false,
    //   album       : 'Absolution',
    //   duration : 0,
    //   elapsed : 0,
    //   ticker    : 'Now playing "Time is Running Out"'
    // });
    //
    // this.musicControls.subscribe().subscribe((action: any) => {
    //   console.log(action);
    //   const message = JSON.parse(action).message || '';
    //   switch(message) {
    //     // External controls (Android only)
    //     case 'music-controls-pause':
    //       this.playerService.setPlaying(false);
    //       break;
    //     case 'music-controls-play':
    //       this.playerService.setPlaying(true);
    //       break;
    //
    //     // External controls (iOS only)
    //     case 'music-controls-toggle-play-pause' :
    //       if (this.playing) {
    //         this.playerService.setPlaying(true);
    //       } else {
    //         this.playerService.setPlaying(false);
    //       }
    //       break;
    //
    //     default:
    //       break;
    //   }
    //
    //   this._ngZone.run(() => {console.log('Outside Done!') });
    // });
    //
    // this.musicControls.listen();
    // this.playerService.setPlaying(true);
    
    
    
    let modal = this.modalCtrl.create(PlayModal);
    modal.present();
  }

  checkInetConnection() {
    let toast = this.toastCtrl.create({
      message: 'There is no internet connection',
      duration: 2000
    });
    toast.present();
  }
}
