import {NgModule, ErrorHandler} from "@angular/core";
import { Geolocation } from '@ionic-native/geolocation';
import {IonicApp, IonicModule, IonicErrorHandler} from "ionic-angular";
import {MyApp} from "./app.component";
import {StatusBar} from "@ionic-native/status-bar";
import {SplashScreen} from "@ionic-native/splash-screen";
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';
import {BrowserModule} from "@angular/platform-browser";
import {InlineSVGModule} from "ng-inline-svg";
import { MusicControls } from '@ionic-native/music-controls';
import { LocalNotifications } from '@ionic-native/local-notifications';
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
import {EventsPage} from "../pages/events/events";
import {ContactPage} from "../pages/contact/contact";
import {HomePage} from "../pages/home/home";
import {EventsArticlePage} from "../pages/events-article/events-article";
import {JobsPage} from "../pages/jobs/jobs";
import {JobsArticlePage} from "../pages/jobs-article/jobs-article";
import {NewsPage} from "../pages/news/news";
import {NewsArticlePage} from "../pages/news-article/news-article";
import {PrivacyPage} from "../pages/policy/policy";
import {ArtistsPage} from "../pages/artists/artists";
import {ArtistsArticlePage} from "../pages/artists-article/artists-article";
import {ArtistsRadioShowsPage} from "../pages/artists-radio-shows/artists-radio-shows";
import {ShowsPage} from "../pages/shows/shows";
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
import {PlayModal} from "../modals/play/play";
import {StationsModal} from "../modals/stations/stations";
import {FilterModal} from "../modals/filter/filter";
import {ContactsModal} from "../modals/contacts/contacts";
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
import {FilterPipe} from "../pipes/filter.pipe";
import {EventsPipe} from "../pipes/events.pipe";
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
import {PlayerService} from "../services/player.services";
import {BackDirective} from "../directives/back";
import {ApiService} from "../services/api.services";
import {CommonService} from "../services/common.services";
import {Http, HttpModule} from "@angular/http";
import {FormsModule} from "@angular/forms";
import {TruncatePipe} from "../pipes/truncate";
import {ChartComponent} from "../modals/chart/chart";
import {DatePipe} from "@angular/common";



const cloudSettings: CloudSettings = {
  'core': {
    'app_id': 'e5c5fd916cc0f169d6c742ef52f7bec5eeffcac327543f76'
  }
};




@NgModule({
  declarations: [
    MyApp,
    EventsPage,
    ContactPage,
    HomePage,
    EventsArticlePage,
    JobsPage,
    JobsArticlePage,
    NewsPage,
    NewsArticlePage,
    ArtistsPage,
    ArtistsArticlePage,
    ArtistsRadioShowsPage,
    PrivacyPage,
    ShowsPage,
    FilterModal,
    PlayModal,
    StationsModal,
    ContactsModal,
    EventsPipe,
    FilterPipe,
    BackDirective,
    TruncatePipe,
    ChartComponent
  ],
  imports: [
    HttpModule,
    FormsModule,
    BrowserModule,
    InlineSVGModule,
    IonicModule.forRoot(MyApp),
    CloudModule.forRoot(cloudSettings)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    EventsPage,
    ContactPage,
    HomePage,
    EventsArticlePage,
    JobsPage,
    JobsArticlePage,
    NewsPage,
    NewsArticlePage,
    ArtistsPage,
    ArtistsArticlePage,
    ArtistsRadioShowsPage,
    PrivacyPage,
    ShowsPage,
    FilterModal,
    PlayModal,
    StationsModal,
    ContactsModal,
    ChartComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    MusicControls,
    PlayerService,
    CommonService,
    ApiService,
    LocalNotifications,
    DatePipe,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {
}


