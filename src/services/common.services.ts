import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs";


@Injectable()
export class CommonService {
  currentSong = new BehaviorSubject<any>({});
  
  constructor(
  ) {
  
  }
  
  
  
  
  get currentSongAsync() {
    return this.currentSong.asObservable().distinctUntilChanged();
  }
  setCurrentSong(a) {
    this.currentSong.next(a);
  }
  
  
  
  objOfobjts2Arr(oofo) {
    let ar = [];
    for(let item in oofo){
      ar.push(oofo[item]);
    }
    return ar;
  }
  
  findObjInArr(arr, key, value){
    for (let i = 0; i < arr.length; i++) {
      if (arr[i][key] === value) {
        return arr[i];
      }
    }
  }
  
  shuffleArr(array) {
    let currentIndex = array.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }
}
