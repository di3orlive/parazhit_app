import {Injectable} from "@angular/core";
import {Http} from '@angular/http';
import {Observable} from "rxjs";



@Injectable()
export class ApiService {
  api_url = 'https://api.parazhit.com/api/';
  
  constructor(
    private http: Http
  ) {
  
  }
  
  
  checkParams(obj) {
    return JSON.parse(JSON.stringify(obj,
      function(k, v) { if (v === null) { return undefined; } return v; }
    ));
  }
  
  
  checkForErr(err) {
    return Observable.throw(err).catch(err => Observable.of(err));
  }
  
  
  
  
  
  getP(path) {
    return this.http.get(`${this.api_url}${path}`)
      .map((res) => res.json())
      .catch((err: any) => this.checkForErr(err));
  }
  
  getPB(path, body) {
    return this.http.get(`${this.api_url}${path}`, {params: this.checkParams(body)})
      .map((res) => res.json())
      .catch((err: any) => this.checkForErr(err));
  }
  
  postPB(path, body) {
    return this.http.post(`${this.api_url}${path}`, this.checkParams(body))
      .map((res) => res.json())
      .catch((err: any) => this.checkForErr(err));
  }
  
  putPB(path, body) {
    return this.http.put(`${this.api_url}${path}`, this.checkParams(body))
      .map((res) => res.json())
      .catch((err: any) => this.checkForErr(err));
  }
  
  deleteP(path) {
    return this.http.delete(`${this.api_url}${path}`)
      .map((res) => res.json())
      .catch((err: any) => this.checkForErr(err));
  }
  
  
  getDayShows(params) {
    let calendar = 'https://www.googleapis.com/calendar/v3/calendars/f77bsa3sv77fodb1htf5pic020%40group.calendar.google.com';
    let apiKey = 'AIzaSyDadOKcH6H7PZ528P8p-pwrm-sSD5drDPg';
    
    const body = {
      timeMin: params.timeMin,
      timeMax: params.timeMax,
      singleEvents: true,
      orderBy: 'startTime'
    };
    
    return this.http.get(`${calendar}/events?key=${apiKey}`, {params: this.checkParams(body)})
      .map((res) => res.json())
      .catch((err: any) => this.checkForErr(err));
  }
}





// GET = albums/:album_id
// GET = albums

// GET = artists/:artist_id?getNews=7&getEvents=8
// GET = artists?offset=:offset_val&limit=:limit_val
// POST = artists/search?limit=1
// data: {
//   name: '@name'
// }

// var calendar = 'https://www.googleapis.com/calendar/v3/calendars/f77bsa3sv77fodb1htf5pic020%40group.calendar.google.com';
// var apiKey = 'AIzaSyDadOKcH6H7PZ528P8p-pwrm-sSD5drDPg';
// getDayShows: {
//   method: "GET",
//     url: calendar + '/events?key=' + apiKey,
//     isArray: false,
//     params: {
//     timeMin: '@timeMin',
//       timeMax: '@timeMax',
//       singleEvents: true,
//       orderBy: 'startTime'
//   }
// }

// POST = contact
// data: {
//   name: '@name',
//   mail: '@mail',
//   phone: '@phone',
//   category: '@category',
//   radio: '@radio',
//   message: '@message',
//   file_data: '@file_data',
//   file_name: '@file_name'
// }

// GET = events/:event_id?getArtists=1
// GET = events?getArtists=1&offset=:offset_val&limit=:limit_val
// GET = events/types/list

// GET = jobs/:job_id
// GET = jobs

// GET = continents
// GET = tracks/:artist_name/:song_name
// GET = homepage

// GET = news/:news_id?getSimilar=7
// GET = news?onTop=1

// GET = tracks/:track_id
// GET = tracks

// GET = tracks/playHistory
// GET = tracks/currentSong
// GET = tracks/top/:top_name', //spotify or parazhit
// POST = tracks/upload
