import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs";
import { MusicControls } from '@ionic-native/music-controls';


@Injectable()
export class PlayerService {
  player: any;
  playing = new BehaviorSubject<any>(false);
  
  constructor(
    private musicControls: MusicControls
  ) {
  
  }
  
  
  setPlayer(player){
    this.player = player;
  }
  pause(){
    this.player.nativeElement.pause();
    this.musicControls.updateIsPlaying(false);
  }
  play(){
    this.player.nativeElement.play();
    this.musicControls.updateIsPlaying(true);
  }
  
  
  get playingAsync() {
    return this.playing.asObservable().distinctUntilChanged();
  }
  setPlaying(a) {
    if (a) {
      this.play();
    } else {
      this.pause();
    }
    
    this.playing.next(a);
  }
}
