import { Component } from '@angular/core';
import {NavParams, ViewController} from 'ionic-angular';


@Component({
  selector: 'filter',
  templateUrl: 'filter.html'
})
export class FilterModal {
  private filter: any;
  private filterArgs = {
    type: 'All',
    location: 'All'
  };

  constructor(
    public params: NavParams,
    public viewCtrl: ViewController
  ) {
    this.filter = this.params.get('filter');
    if(this.filter){
      this.checkFilter();
    }
  }



  setFilter(arr, item) {
    arr.forEach(function (elem) {
      elem.isActive = false;
    });

    item.isActive = true;

    this.checkFilter();
  }


  checkFilter(){
    this.filter.event_type.forEach(item => {
       if(item.isActive){
         this.filterArgs.type = item.name;
       }
    });


    this.filter.location.forEach(item => {
      if(item.isActive){
        this.filterArgs.location = item.name;
      }
    });
  }


  dismiss() {
    this.viewCtrl.dismiss({
      type: this.filterArgs.type,
      location: this.filterArgs.location
    });
  }
}


