import { Component, NgZone } from '@angular/core';
import {ViewController, ModalController} from 'ionic-angular';
import {StationsModal} from "../stations/stations";
import {PlayerService} from "../../services/player.services";
import {SafeSubscribe} from "../../helpers/safe-subscripe/safe-subscripe";
import {ApiService} from "../../services/api.services";
import {CommonService} from "../../services/common.services";


@Component({
  selector: 'play',
  templateUrl: 'play.html'
})
export class PlayModal extends SafeSubscribe {
  playing: any;
  currentSong: any;
  tweets: any;

  
  constructor(
    private _ngZone: NgZone,
    private playerService: PlayerService,
    private modalCtrl: ModalController,
    public viewCtrl: ViewController,
    private api: ApiService,
    private common: CommonService
  ) {
    super();
    
    this.playerService.playingAsync.safeSubscribe(this, (value) => {
      this.playing = value;
    });
    
    
    
    this.getCurrentSong(() => {
      this.api.postPB('artists/search?limit=1', {
        name: this.currentSong.artist,
        radioId: 1,
      }).safeSubscribe(this, (res: any) => {
        if (res && res.tweets) {
          this.tweets = JSON.parse(res.tweets)
          console.log(this.tweets);
        }
      });
    });
    setInterval(() => {
      this.getCurrentSong();
    }, 5000);
  }
  
  
  getCurrentSong(cb?) {
    this.api.getP('tracks/currentSong?radioId=1').safeSubscribe(this, (res: any) => {
      this.currentSong = res;
      
      this.common.setCurrentSong(this.currentSong);
      
      if (cb) {
        cb();
      }
    });
  }
  
  
  toggle() {
    this.playerService.setPlaying(!this.playing);
    this._ngZone.run(() => {console.log('Outside Done!') });
  }
  
  dismiss(){
    this.playerService.setPlaying(this.playing);
    this._ngZone.run(() => {console.log('Outside Done!') });
    this.viewCtrl.dismiss();
  }

  stationsModal(){
    let modal = this.modalCtrl.create(StationsModal, {tweets: this.tweets});
    modal.present();
  }
}


