import { Component } from '@angular/core';
import {NavParams, ViewController} from 'ionic-angular';


@Component({
  selector: 'contacts-modal',
  templateUrl: 'contacts.html'
})
export class ContactsModal {
  private list: any;
  private selected: any;


  constructor(
    public params: NavParams,
    public viewCtrl: ViewController
  ) {
    this.list = this.params.get('list');
  }



  itemSelected(item) {
    this.selected = item;
  }

  dismiss() {
    this.viewCtrl.dismiss(this.selected);
  }
}
