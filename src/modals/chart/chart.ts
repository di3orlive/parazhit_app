import { Component } from '@angular/core';
import {NavParams, ViewController} from "ionic-angular";

@Component({
  selector: 'chart',
  templateUrl: 'chart.html'
})
export class ChartComponent {
  activeChart: any;
  charts = [
    {name: 'Parazhit', isActive: false},
    {name: 'Spotify', isActive: false}
  ];

  constructor(
    public viewCtrl: ViewController,
    public params: NavParams
  ) {
    this.activeChart = this.params.get('activeChart');
  }
  
  
  ionViewDidLoad() {
    this.charts.forEach((item) => {
      if (item.name == this.activeChart) {
        item.isActive = true;
      }
    });
  }
  

  dismiss() {
    this.viewCtrl.dismiss();
  }
  

  setChart(item) {
    this.charts.forEach((it) => {
      it.isActive = false;
    });
  
    item.isActive = true;
    this.viewCtrl.dismiss(item.name);
  }
}
