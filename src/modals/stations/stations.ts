import { Component } from '@angular/core';
import {ViewController, Platform, NavParams} from 'ionic-angular';
import {CommonService} from "../../services/common.services";
import {SafeSubscribe} from "../../helpers/safe-subscripe/safe-subscripe";


@Component({
  selector: 'stations',
  templateUrl: 'stations.html'
})
export class StationsModal extends SafeSubscribe{
  currentSong: any;
  tweets: any;

  constructor(
    public viewCtrl: ViewController,
    public params: NavParams,
    private common: CommonService
  ) {
    super();
    
    this.tweets = this.params.get('tweets');
  
    console.log(this.tweets);
  
    this.common.currentSongAsync.safeSubscribe(this, (res) => {
      this.currentSong = res;
    });
  }


  dismiss() {
    this.viewCtrl.dismiss();
  }
}


