import {Component} from "@angular/core";
import {NavParams} from "ionic-angular";
import {ApiService} from "../../services/api.services";
import {SafeSubscribe} from "../../helpers/safe-subscripe/safe-subscripe";

@Component({
  selector: 'news-article',
  templateUrl: 'news-article.html'
})
export class NewsArticlePage extends SafeSubscribe {
  newsId: any;
  news: any;

  constructor(
    private api: ApiService,
    public navParams: NavParams,
  ) {
    super();
    this.newsId = this.navParams.get('newsId');
    console.log(this.newsId);
  }
  
  ionViewDidLoad() {
    this.getNews(this.newsId);
  }
  
  getNews(id) {
    this.api.getP(`news/${id}?getSimilar=7&radi`).safeSubscribe(this, (res: any) => {
      this.news = res;
      console.log(this.news);
    });
  }
}
