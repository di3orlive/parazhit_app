import {Component} from "@angular/core";
import {NavController} from "ionic-angular";
import {DatePipe} from "@angular/common";
import {ApiService} from "../../services/api.services";
import {SafeSubscribe} from "../../helpers/safe-subscripe/safe-subscripe";

@Component({
  selector: 'shows',
  templateUrl: 'shows.html'
})
export class ShowsPage extends SafeSubscribe {
  week = {
    arr: [],
    cur: {id: 0}
  };

  constructor(
    private api: ApiService,
    public nav: NavController,
    private datePipe: DatePipe
  ) {
    super();
    this.initWeek();
  }
  
  
  ionViewDidLoad() {
    this.getDayShows();
  }

  initWeek() {
    for(let i = 0; i < 7; i++){
      this.week.arr.push({
        id: i,
        date: new Date(new Date().setDate(new Date().getDate() + (i - 1))),
        isActive: i == 1
      })
    }
    this.week.cur = this.week.arr[1];
    console.log(this.week.cur);
  }

  setDay(item:any):void{
    this.week.cur = this.week.arr[item.id];

    this.week.arr.forEach((it) => {
      it.isActive = false;
    });

    item.isActive = true;
    
    this.getDayShows();
  }
  
  
  getDayShows() {
    let timezone = this.datePipe.transform(new Date(), 'Z');
    timezone = timezone.substr(0, 3) + ':00';
    let timeMin = this.datePipe.transform(new Date(new Date().setDate(new Date().getDate() + (this.week.cur.id - 1))), 'yyyy-MM-dd');
    let timeMax = this.datePipe.transform(new Date(new Date().setDate(new Date().getDate() + (+this.week.cur.id))), 'yyyy-MM-dd');
    timeMax += 'T00:00:00' + timezone;
    timeMin += 'T00:00:00' + timezone;
  
    const params = {
      timeMin: timeMin,
      timeMax: timeMax
    };
    

    this.api.getDayShows(params).safeSubscribe(this, (res: any) => {
  
      console.log(res);
  
      // $scope.shows = data.items;
      //
      // $scope.shows.sort(function (a, b) {
      //   return (+new Date(a.start.dateTime)) - (+new Date(b.start.dateTime))
      // });
    });
  }
}
