import {Component} from "@angular/core";
import {NavController} from "ionic-angular";
import {JobsArticlePage} from "../jobs-article/jobs-article";
import {ApiService} from "../../services/api.services";
import {SafeSubscribe} from "../../helpers/safe-subscripe/safe-subscripe";

@Component({
  selector: 'jobs',
  templateUrl: 'jobs.html'
})
export class JobsPage extends SafeSubscribe {
  jobs = [];

  constructor(
    private api: ApiService,
    public nav: NavController
  ) {
    super();
  }
  
  
  ionViewDidLoad() {
    this.api.getP('jobs?radioId=1').safeSubscribe(this, (res: any) => {
      this.jobs = res.rows;
  
      console.log(this.jobs);
    });
  }
  
  
  openJob(i){
    this.nav.push(JobsArticlePage, {jobId: i.id});
  }
}
