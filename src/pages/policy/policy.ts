import {Component} from "@angular/core";
import {NavParams, NavController} from "ionic-angular";

@Component({
  selector: 'policy',
  templateUrl: 'policy.html'
})
export class PrivacyPage {
  private article: any;

  constructor(
    public navParams: NavParams,
    public nav: NavController,
  ) {
    this.article = this.navParams.get('article')
  }




  historyBack(){
    this.nav.pop();
  }
}
