import {Component} from "@angular/core";
import {NavParams, NavController} from "ionic-angular";
import {ApiService} from "../../services/api.services";
import {SafeSubscribe} from "../../helpers/safe-subscripe/safe-subscripe";
import {ArtistsPage} from "../artists/artists";
import {ContactPage} from "../contact/contact";

@Component({
  selector: 'jobs-article',
  templateUrl: 'jobs-article.html'
})
export class JobsArticlePage extends SafeSubscribe {
  jobId: any;
  job: any;

  constructor(
    private api: ApiService,
    public navParams: NavParams,
    public nav: NavController,
  ) {
    super();
    this.jobId = this.navParams.get('jobId');
  }
  
  
  ionViewDidLoad() {
    this.api.getP(`jobs/${this.jobId}?radioId=1`).safeSubscribe(this, (res: any) => {
      this.job = res;
      
      console.log(this.job);
    });
  }


  historyBack(){
    this.nav.pop();
  }
  
  
  openContacts(){
    this.nav.push(ContactPage, {jobTitle: this.job.title});
  }
}
