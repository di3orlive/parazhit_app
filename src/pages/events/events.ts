import {NavController, ModalController} from "ionic-angular";
import {Component} from "@angular/core";
import {FilterModal} from "../../modals/filter/filter";
import {EventsArticlePage} from "../events-article/events-article";
import {ApiService} from "../../services/api.services";
import {SafeSubscribe} from "../../helpers/safe-subscripe/safe-subscripe";

@Component({
  selector: 'events-page',
  templateUrl: 'events.html'
})
export class EventsPage extends SafeSubscribe {
  blurOnFilter = false;
  filterArgs = {
    type: 'All',
    location: 'All'
  };
  
  events = [];

  filter = {
    event_type: [
      {name: 'All', isActive: true},
      {name: 'Concert', isActive: false},
      {name: 'Dayclub', isActive: false},
      {name: 'Festival', isActive: false},
      {name: 'Nightclub', isActive: false},
      {name: 'Pool Party', isActive: false},
      {name: 'Showcase', isActive: false}
    ],
    location: [
      {name: 'All', isActive: true},
      {name: 'Africa', isActive: false},
      {name: 'Asia', isActive: false},
      {name: 'Australia', isActive: false},
      {name: 'Europe', isActive: false},
      {name: 'North America', isActive: false},
      {name: 'South America', isActive: false},
    ]
  };

  data = [
    {type: 'Festival', location: 'Australia'},
    {type: 'Dayclub', location: 'Africa'},
    {type: 'Showcase', location: 'Asia'},
    {type: 'Showcase', location: 'Australia'},
    {type: 'Concert', location: 'Europe'},
    {type: 'Concert', location: 'North America'},
    {type: 'Concert', location: 'South America'},
  ];



  constructor(
    public nav: NavController,
    private api: ApiService,
    private modalCtrl: ModalController
  ) {
    super();
  }
  
  
  ionViewDidLoad() {
    this.api.getP('events?getArtists=1&offset=0&limit=24&radioId=1').safeSubscribe(this, (res: any) => {
      this.events = res.rows;
    });
  }


  openEvent(i){
    this.nav.push(EventsArticlePage, {eventId: i.id});
  }


  filterModal() {
    this.blurOnFilter = true;
    let modal = this.modalCtrl.create(FilterModal, {filter: this.filter});
    modal.onDidDismiss(data => {
      this.blurOnFilter = false;
      this.filterArgs = {
        type: data.type,
        location: data.location
      };
      console.log(this.filterArgs);
    });
    modal.present();
  }

}
