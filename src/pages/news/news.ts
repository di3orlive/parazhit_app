import {Component} from "@angular/core";
import {NavController} from "ionic-angular";
import {NewsArticlePage} from "../news-article/news-article";
import {ApiService} from "../../services/api.services";
import {SafeSubscribe} from "../../helpers/safe-subscripe/safe-subscripe";
import {CommonService} from "../../services/common.services";

@Component({
  selector: 'news',
  templateUrl: 'news.html'
})
export class NewsPage extends SafeSubscribe {
  news = [];
  highlighted: any;

  constructor(
    private api: ApiService,
    private common: CommonService,
    public nav: NavController
  ) {
    super();
  }
  
  ionViewDidLoad() {
    this.api.getP('news?onTop=1&radioId=1').safeSubscribe(this, (res: any) => {
      this.news = res.rows;
  
      this.highlighted = this.common.shuffleArr(this.news);
      console.log(this.highlighted);
    });
  }


  openNews(i){
    this.nav.push(NewsArticlePage, {newsId: i.id});
  }
}
