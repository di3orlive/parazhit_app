import {Component} from "@angular/core";
import {NavParams, NavController} from "ionic-angular";
import {ArtistsRadioShowsPage} from "../artists-radio-shows/artists-radio-shows";
import {ApiService} from "../../services/api.services";
import {SafeSubscribe} from "../../helpers/safe-subscripe/safe-subscripe";

@Component({
  selector: 'events-article',
  templateUrl: 'events-article.html'
})
export class EventsArticlePage extends SafeSubscribe {
  eventId: any;
  event: any;

  constructor(
    public nav: NavController,
    public navParams: NavParams,
    public navController: NavController,
    private api: ApiService,
  ) {
    super();
    this.eventId = this.navParams.get('eventId')
  }
  
  
  ionViewDidLoad() {
    this.getEvent(this.eventId);
  }


  getEvent(id) {
    this.api.getP(`events/${id}?getArtists=1&radioId=1`).safeSubscribe(this, (res: any) => {
      this.event = res;
      console.log(this.event);
    });
  }

  openRadioShows() {
    this.nav.push(ArtistsRadioShowsPage, {shows: ''});
  }


  
  openLink(url) {
    window.open(url, '_system');
  }
}
