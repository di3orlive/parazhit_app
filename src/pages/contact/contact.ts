import {Component, ElementRef, ViewChild} from '@angular/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {ModalController, NavParams} from 'ionic-angular';
import {ContactsModal} from "../../modals/contacts/contacts";
import {ApiService} from "../../services/api.services";
import {SafeSubscribe} from "../../helpers/safe-subscripe/safe-subscripe";

declare let google;

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage extends SafeSubscribe{
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  private form : FormGroup;
  categoryList = [
    'General',
    'Editorial / Concert Inquiries',
    'Press / Media Inquiries',
    'Advertising',
    'Join Our Team',
    'Merchandise / Shop',
    'Events',
    'Website / Webmaster',
    'Partnership / Sponsorship'
  ];
  radioList = ['Parazhit','Earhit','Gravihit','Oppozhit','Xzhit'];




  constructor(
    private formBuilder: FormBuilder,
    private api: ApiService,
    public navParams: NavParams,
    public modalCtrl: ModalController
  ) {
    super();
    
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      mail: ['', Validators.required],
      phone: ['', Validators.required],
      category: [''],
      radio: [''],
      message: [''],
      agree: [false, Validators.required],
    });
  
  
    if (this.navParams.get('jobTitle')) {
      // TODO: add title to select
    }
  }


  openModal(list, type){
    let modal = this.modalCtrl.create(ContactsModal, {list:list});
    modal.present();
    modal.onDidDismiss(data => {

      if(type == 'category'){
        this.form.controls['category'].setValue(data);
      }else if(type == 'radio'){
        this.form.controls['radio'].setValue(data);
      }

    });
  }


  submitForm(){
    if (this.form.invalid) {return null;}
    
    const body = {
      name: this.form.value.name,
      mail: this.form.value.mail,
      phone: this.form.value.phone,
      category: this.form.value.category,
      radio: this.form.value.radio,
      message: this.form.value.message
    };
    // POST = contact
    // data: {
    // }
    console.log(body);
  
  
    this.api.postPB('contact', body).safeSubscribe(this, (res: any) => {
      console.log(res);
    });
  }


  ionViewWillEnter(): void{
    this.loadMap();
  }

  private loadMap(): void{
    let latLng = new google.maps.LatLng(52.369323,4.901122);

    let mapOptions = {
      center: latLng,
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true,
      styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"landscape","elementType":"labels.icon","stylers":[{"saturation":"-100"},{"lightness":"-54"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"on"},{"lightness":"0"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"saturation":"-89"},{"lightness":"-55"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"transit.station","elementType":"labels.icon","stylers":[{"visibility":"on"},{"saturation":"-100"},{"lightness":"-51"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    let marker = new google.maps.Marker({
      position: latLng,
      icon: 'assets/dot.png',
    });

    marker.setMap(this.map);
  }
}
