import { Component } from '@angular/core';
import {ModalController, NavController} from "ionic-angular";
import {NewsArticlePage} from "../news-article/news-article";
import {EventsArticlePage} from "../events-article/events-article";
import {ApiService} from "../../services/api.services";
import {SafeSubscribe} from "../../helpers/safe-subscripe/safe-subscripe";
import {ArtistsPage} from "../artists/artists";
import {CommonService} from "../../services/common.services";
import {ChartComponent} from "../../modals/chart/chart";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage extends SafeSubscribe {
  main: any;
  top: any;
  news: any;
  
  blurOnFilter: boolean;
  chartTopFilter = 'Parazhit';
  chartTop: any;
  
  events: any;
  

  constructor(
    public nav: NavController,
    private common: CommonService,
    private api: ApiService,
    public modalCtrl: ModalController
  ) {
    super();
  }
  
  ionViewDidLoad() {
    this.api.getP('homepage?radioId=1').safeSubscribe(this, (res: any) => {
      this.main = res.sliders.main[1];
      this.top = {
        highlighted: this.common.objOfobjts2Arr(res.topTracks.highlighted),
        parazhit: this.common.objOfobjts2Arr(res.topTracks.parazhit),
        spotify: this.common.objOfobjts2Arr(res.topTracks.spotify)
      };
      this.chartTop = this.top.parazhit;
    });
    
    
    this.api.getP('news?onTop=1&radioId=1').safeSubscribe(this, (res: any) => {
      this.news = res.rows;
    });
    
    
    this.api.getP('events?getArtists=1&offset=0&limit=24&radioId=1').safeSubscribe(this, (res: any) => {
      this.events = res.rows;
    });
  }
  
  
  openResidents(){
    this.nav.push(ArtistsPage);
  }
  openNews(i){
    this.nav.push(NewsArticlePage, {newsId: i.id});
  }
  openEvent(i){
    this.nav.push(EventsArticlePage, {eventId: i.id});
  }


  setChartTopFilter() {
    this.blurOnFilter = true;
    let chartModal = this.modalCtrl.create(ChartComponent, { activeChart: this.chartTopFilter });
  
    chartModal.onDidDismiss(res => {
      this.blurOnFilter = false;
      if (!!res) {
        this.chartTopFilter = res;
        if (res === 'Parazhit') {
          this.chartTop = this.top.parazhit;
        } else if (res === 'Spotify') {
          this.chartTop = this.top.spotify;
        }
      }
    });
    chartModal.present();
  }

}
