import {Component, ViewChild} from "@angular/core";
import {App, Content, NavController, NavParams} from "ionic-angular";
import {ArtistsRadioShowsPage} from "../artists-radio-shows/artists-radio-shows";
import {ApiService} from "../../services/api.services";
import {SafeSubscribe} from "../../helpers/safe-subscripe/safe-subscripe";
import {CommonService} from "../../services/common.services";

@Component({
  selector: 'artists-article',
  templateUrl: 'artists-article.html'
})
export class ArtistsArticlePage extends SafeSubscribe {
  artistId: any;
  artist: any;
  artists: any;
  biographyLimit = 300;
  tweetsLimit = 3;

  constructor(
    public navParams: NavParams,
    public nav: NavController,
    private api: ApiService,
    private common: CommonService
  ) {
    super();
    this.artistId = this.navParams.get('artistId');
    this.artists = this.navParams.get('artists');
  }
  
  ionViewDidLoad() {
    this.getArtist(this.artistId);
  }

  openRadioShows() {
    this.nav.push(ArtistsRadioShowsPage, {shows: ''});
  }
  
  getArtist(id) {
    this.api.getP(`artists/${id}?getNews=7&getEvents=8&radioId=1`).safeSubscribe(this, (res: any) => {
      this.artist = res;
      this.artist.tweets = JSON.parse(this.artist.tweets);
    });
  }

  
  openLink(url) {
    window.open(url, '_system');
  }
  
  toggleBiography() {
    this.biographyLimit = this.biographyLimit == 300 ? 10000 : 300;
  }
  
  toggleTweets() {
    this.tweetsLimit = this.tweetsLimit == 3 ? 100 : 3;
  }
  
  openResident(i) {
    this.artistId = i.id;
    this.nav.push(ArtistsArticlePage, {artistId: this.artistId, artists: this.artists});
  }
}
