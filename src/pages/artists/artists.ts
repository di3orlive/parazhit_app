import {Component} from "@angular/core";
import {NavController} from "ionic-angular";
import {ArtistsArticlePage} from "../artists-article/artists-article";
import {SafeSubscribe} from "../../helpers/safe-subscripe/safe-subscripe";
import {ApiService} from "../../services/api.services";

@Component({
  selector: 'artists-page',
  templateUrl: 'artists.html'
})
export class ArtistsPage extends SafeSubscribe {
  letters = [
    {name: 'A', isActive: false},
    {name: 'B', isActive: false},
    {name: 'C', isActive: false},
    {name: 'D', isActive: false},
    {name: 'E', isActive: false},
    {name: 'F', isActive: false},
    {name: 'G', isActive: false},
    {name: 'H', isActive: false},
    {name: 'I', isActive: false},
    {name: 'J', isActive: false},
    {name: 'K', isActive: false},
    {name: 'L', isActive: false},
    {name: 'M', isActive: false},
    {name: 'N', isActive: false},
    {name: 'O', isActive: false},
    {name: 'P', isActive: false},
    {name: 'Q', isActive: false},
    {name: 'R', isActive: false},
    {name: 'S', isActive: false},
    {name: 'T', isActive: false},
    {name: 'U', isActive: false},
    {name: 'V', isActive: false},
    {name: 'W', isActive: false},
    {name: 'X', isActive: false},
    {name: 'Y', isActive: false},
    {name: 'Z', isActive: false},
    {name: '#', isActive: false}
  ];
  artists = [];

  constructor(
    public nav: NavController,
    private api: ApiService
  ) {
    super();

  }
  
  ionViewDidLoad() {
    this.api.getP('artists?offset=0&limit=24&radioId=1').safeSubscribe(this, (res: any) => {
      this.artists = res.rows;
    });
  }


  search(letter) {
    let isTrue = letter.isActive;

    this.letters.forEach(item => item.isActive = false);

    letter.isActive = !isTrue;
  }

  openArtist(i) {
    this.nav.push(ArtistsArticlePage, {artistId: i.id, artists: this.artists});
  }


  historyBack(){
    this.nav.pop();
  }
}
