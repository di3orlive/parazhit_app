import {Component, ElementRef, ViewChild} from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import {NavParams, Content} from 'ionic-angular';

declare let google;

@Component({
  selector: 'artists-radio-shows',
  templateUrl: 'artists-radio-shows.html'
})
export class ArtistsRadioShowsPage {
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild(Content) content: Content;
  private map: any;
  private article: any;
  private mapOptions = {
    center: null,
    zoom: 13,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true,
    styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"landscape","elementType":"labels.icon","stylers":[{"saturation":"-100"},{"lightness":"-54"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"on"},{"lightness":"0"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"saturation":"-89"},{"lightness":"-55"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"transit.station","elementType":"labels.icon","stylers":[{"visibility":"on"},{"saturation":"-100"},{"lightness":"-51"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]
  };
  private timetable = [
    {id: '0', city: 'You\'re ', country: 'here', lat: 0, lng: 0, timeZoneInMs: 0, isActive: true, dateTime: null},
    {id: '1', city: 'Los Angeles', country: 'United States', lat: 34.047495, lng: -118.245594, timeZoneInMs: -28800000, isActive: false, dateTime: null},
    {id: '2', city: 'Mexico City', country: 'Mexico', lat: 19.436012, lng: -99.129202, timeZoneInMs: -21600000, isActive: false, dateTime: null},
    {id: '3', city: 'Miami', country: 'United States', lat: 25.763139, lng: -80.191867, timeZoneInMs: -18000000, isActive: false, dateTime: null},
    {id: '4', city: 'New York', country: 'United States', lat: 40.712852, lng: -74.010282, timeZoneInMs: -18000000, isActive: false, dateTime: null},
    {id: '5', city: 'São Paulo', country: 'Brazil', lat: -23.548782, lng: -46.647517, timeZoneInMs: -10800000, isActive: false, dateTime: null},
    {id: '6', city: 'Lagos', country: 'Nigeria', lat: 6.522747, lng: 3.378515, timeZoneInMs: 3600000, isActive: false, dateTime: null},
    {id: '7', city: 'London', country: 'United Kingdom', lat: 51.518305, lng: -0.130443, timeZoneInMs: 0, isActive: false, dateTime: null},
    {id: '8', city: 'Amsterdam', country: 'Netherlands', lat: 52.372495, lng: 4.894836, timeZoneInMs: 3600000, isActive: false, dateTime: null},
    {id: '9', city: 'Paris', country: 'France', lat: 48.857031, lng: 2.350989, timeZoneInMs: 3600000, isActive: false, dateTime: null},
    {id: '10', city: 'Ibiza', country: 'Spain', lat: 38.908262, lng: 1.429304, timeZoneInMs: 3600000, isActive: false, dateTime: null},
    {id: '11', city: 'Stockholm', country: 'Sweden', lat: 59.329325, lng: 18.068581, timeZoneInMs: 3600000, isActive: false, dateTime: null},
    {id: '12', city: 'Cape Town', country: 'South Africa', lat: -33.933854, lng: 18.439051, timeZoneInMs: 7200000, isActive: false, dateTime: null},
    {id: '13', city: 'Istanbul', country: 'Turkey', lat: 41.010418, lng: 28.964108, timeZoneInMs: 10800000, isActive: false, dateTime: null},
    {id: '14', city: 'Moscow', country: 'Russia', lat: 55.755735, lng: 37.617175, timeZoneInMs: 10800000, isActive: false, dateTime: null},
    {id: '15', city: 'Mumbai', country: 'India', lat: 19.072387, lng: 72.872269, timeZoneInMs: 19800000, isActive: false, dateTime: null},
    {id: '16', city: 'Bangkok', country: 'Thailand', lat: 13.745238, lng: 100.547271, timeZoneInMs: 25200000, isActive: false, dateTime: null},
    {id: '17', city: 'Jakarta', country: 'Indonesia', lat: -6.207214, lng: 106.844279, timeZoneInMs: 25200000, isActive: false, dateTime: null},
    {id: '18', city: 'Beijing', country: 'China', lat: 39.911164, lng: 116.408418, timeZoneInMs: 28800000, isActive: false, dateTime: null},
    {id: '19', city: 'Tokyo', country: 'Japan', lat: 35.708300, lng: 139.730860, timeZoneInMs: 32400000, isActive: false, dateTime: null},
    {id: '20', city: 'Bangalore', country: 'India', lat: 12.972027, lng: 77.588359, timeZoneInMs: 19800000, isActive: false, dateTime: null},
    {id: '21', city: 'Seoul', country: 'South Korea', lat: 37.552127, lng: 126.990768, timeZoneInMs: 32400000, isActive: false, dateTime: null},
    {id: '22', city: 'New Delhi', country: 'India', lat: 28.611095, lng: 77.207610, timeZoneInMs: 19800000, isActive: false, dateTime: null},
    {id: '23', city: 'Honolulu', country: 'United States', lat: 21.309469, lng: -157.855723, timeZoneInMs: -36000000, isActive: false, dateTime: null},
    {id: '24', city: 'Anchorage', country: 'United States', lat: 61.211816, lng: -149.899275, timeZoneInMs: -32400000, isActive: false, dateTime: null},
    {id: '25', city: 'Denver', country: 'United States', lat: 39.735219, lng: -104.989014, timeZoneInMs: -25200000, isActive: false, dateTime: null},
    {id: '26', city: 'Chicago', country: 'United States', lat: 41.878142, lng: -87.633285, timeZoneInMs: -21600000, isActive: false, dateTime: null},
    {id: '27', city: 'Havana', country: 'Cuba', lat: 23.113183, lng: -82.368076, timeZoneInMs: -18000000, isActive: false, dateTime: null},
    {id: '28', city: 'Buenos Aires', country: 'Argentina', lat: -34.603982, lng: -58.382660, timeZoneInMs: -10800000, isActive: false, dateTime: null},
    {id: '29', city: 'Reykjavik', country: 'Iceland', lat: 64.125006, lng: -21.822247, timeZoneInMs: 0, isActive: false, dateTime: null},
    {id: '30', city: 'Dublin', country: 'Ireland', lat: 53.349248, lng: -6.260129, timeZoneInMs: 0, isActive: false, dateTime: null},
    {id: '31', city: 'Madrid', country: 'Spain', lat: 40.420099, lng: -3.702889, timeZoneInMs: 3600000, isActive: false, dateTime: null},
    {id: '32', city: 'Dubai', country: 'United Arab Emirates', lat: 25.208558, lng: 55.266549, timeZoneInMs: 14400000, isActive: false, dateTime: null},
    {id: '33', city: 'Karachi', country: 'Pakistan', lat: 24.878082, lng: 67.024871, timeZoneInMs: 18000000, isActive: false, dateTime: null},
    {id: '34', city: 'Almaty', country: 'Kazakhstan', lat: 43.218639, lng: 76.852148, timeZoneInMs: 21600000, isActive: false, dateTime: null},
    {id: '35', city: 'Taipei', country: 'Taiwan', lat: 25.037496, lng: 121.564964, timeZoneInMs: 28800000, isActive: false, dateTime: null},
    {id: '36', city: 'Brisbane', country: 'Australia', lat: -27.458658, lng: 153.028878, timeZoneInMs: 36000000, isActive: false, dateTime: null},
    {id: '37', city: 'Melbourne', country: 'Australia', lat: -37.813718, lng: 144.963656, timeZoneInMs: 36000000, isActive: false, dateTime: null},
    {id: '38', city: 'Suva', country: 'Fiji', lat: -18.124595, lng: 178.448972, timeZoneInMs: 43200000, isActive: false, dateTime: null},
    {id: '39', city: 'Auckland', country: 'New Zealand', lat: -36.856348, lng: 174.768645, timeZoneInMs: 46800000, isActive: false, dateTime: null},
    {id: '40', city: 'Kiritimati', country: 'Kiritimati', lat: 1.878462, lng: -157.429843, timeZoneInMs: 50400000, isActive: false, dateTime: null}
  ];



  constructor(
    public navParams: NavParams,
    private geolocation: Geolocation
  ) {
    this.article = this.navParams.get('shows');
  }




  ionViewWillEnter(): void{
    this.loadMap();
  }


  private loadMap(): void{
    this.geolocation.getCurrentPosition().then(pos => {
      this.timetable[0].lat = pos.coords.latitude;
      this.timetable[0].lng = pos.coords.longitude;
      let latLng = new google.maps.LatLng(this.timetable[0].lat, this.timetable[0].lng);
      this.setMap(latLng)
    }).catch(() => {
      this.timetable.splice(0,1);
      let latLng = new google.maps.LatLng(this.timetable[0].lat,this.timetable[0].lng);
      this.setMap(latLng)
    });
  }


  setMap(latLng){
    for(let i = 0; i < this.timetable.length; i++){
      this.timetable[i].dateTime = new Date(this.timetable[i].timeZoneInMs);
    }


    this.mapOptions.center = latLng;


    this.map = new google.maps.Map(this.mapElement.nativeElement, this.mapOptions);


    let marker;
    for(let i = 0; i < this.timetable.length; i++){
      marker = new google.maps.Marker({
        position: {
          lat: this.timetable[i].lat,
          lng: this.timetable[i].lng
        },
        map: this.map,
        icon: 'assets/dot.png',
        animation: google.maps.Animation.DROP
      });
    }


    marker.addListener('click', () => {
      this.map.setCenter(marker.getPosition());
    });
  }

  setMapCenter(item:any):void{
    this.map.setCenter(new google.maps.LatLng(item.lat,item.lng));
    this.content.scrollToTop();
  }
}
