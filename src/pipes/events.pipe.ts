import {Pipe, Injectable, PipeTransform} from "@angular/core";


@Pipe({
  name: 'eventsPipe'
})
@Injectable()
export class EventsPipe implements PipeTransform {
  transform(arr, filterArgs) {
    if (!arr) return arr;
    let {location,type} = filterArgs;

    return arr.filter(item => {
      return (item.location == location || location == 'All') && (item.type === type || type == 'All')
    });
  }
}
